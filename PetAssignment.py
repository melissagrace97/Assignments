from random import randrange

class Pet():
    hunger_threshold = 5
    boredom_threshold = 5
    boredom_decrement = 4
    hunger_decrement = 4
    sounds = ['meow']
    def __init__(self, name = "Teddy"):
        self.name = name
        self.hunger = randrange(self.hunger_threshold)
        self.boredom = randrange(self.boredom_threshold)
        self.sounds = self.sounds[:]

    def clock_tick(self):
        self.hunger += 1
        self.boredom += 1

    def mood(self):
        if self.boredom > self.boredom_threshold:
            return "bored"
        elif self.hunger > self.hunger_threshold:
            return "hungry"
        else:
            return "happy"

    def __str__(self):
        state = "I am " + self.name + "."
        state += " I feel " + self.mood()
        return state

    def teach(self, word):
        self.sounds.append(word)
        self.reduce_boredom()

    def hi(self):
        print(self.sounds[randrange(len(self.sounds))])
        self.reduce_boredom()

    def feed(self):
        self.reduce_hunger()

    def reduce_boredom(self):
        self.boredom = max(0, self.boredom - self.boredom_decrement)

    def reduce_hunger(self):
        self.hunger = max(0, self.hunger - self.hunger_decrement)

def petexists(petlist, name):
    for pet in petlist:
        if pet.name == name:
            return pet
    return None

def main():
    animals = []
    options = """
        Adopt <petname> 
        Greet <petname>
        Teach <petname> <word>
        Feed <petname>
        Quit
        
        Choice:
    """
    choice = ""
    while(True):
        ch = input(choice + options)
        choice = ""
        words = ch.split()
        if len(words) > 0:
            command = words[0]
        else:
            return None
        if command == "Quit" :
            print("Exit\n")
            return
        elif command == "Adopt" and len(words) > 1 :
            if petexists(animals, words[1]):
                choice += "Pet with that name already exists.\n"
            else:
                animals.append(Pet(words[1]))
        elif command == "Greet" and len(words) > 1:
            pet = petexists(animals, words[1])
            if not pet:
                choice += "Pet doesn't exist\n"
                print()
            else:
                pet.hi()
        elif command == "Teach" and len(words) > 2:
            pet = petexists(animals, words[1])
            if not pet:
                choice += "Pet doesn't exist\n"
            else:
                pet.teach(words[2])
        elif command == "Feed" and len(words) > 1:
            pet = petexists(animals, words[1])
            if not pet:
                choice += "Pet doesn't exist\n"
            else:
                pet.feed()
        else:
            choice += "Try again\n"

        for pet in animals:
            pet.clock_tick()
            choice += "\n" + pet.__str__()

main()

